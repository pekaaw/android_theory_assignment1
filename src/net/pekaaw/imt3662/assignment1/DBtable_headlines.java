package net.pekaaw.imt3662.assignment1;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class DBtable_headlines {
	
	/**
	 * Empty constructor
	 */
	public DBtable_headlines(){}
	
	public static final String DATABASE_TABLE = "headlines";
	
	public static final String ROW_ID = "_ID";
	public static final String TITLE = "title";
	public static final String URL = "code";
	
	/**
	 * Return the string command for creating the table
	 * 
	 * @return String SQL-command
	 */
	public static String createTable() {
		String createSQL = "CREATE TABLE " + DATABASE_TABLE + "(" +
				ROW_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
				TITLE + " TEXT, " +
				URL + " TEXT)";
		
		return createSQL;
	}
	
	/**
	 * Delete the table - return the SQL command
	 * @return String
	 */
	public static String deleteTable() {
		return "DROP TABLE IF EXISTS " + DATABASE_TABLE;
	}
	
	/**
	 * Add a subject to the table
	 * 
	 * @param name
	 * @param code
	 * @param teacher
	 * @return long _id to the inserted row or -1 upon failure
	 */
	public long addEntry( SQLiteDatabase db, String title, String url ) {
		ContentValues values = new ContentValues();
		values.put(TITLE, title);
		values.put(URL, url);
		
		return db.insert(DATABASE_TABLE, null, values);
	}

	/**
	 * Read from table
	 * 
	 * @param id The id
	 */
	public Cursor fetch( SQLiteDatabase db, long id ) {
		return db.query( DATABASE_TABLE, null, ROW_ID + "=" + id, null, null, null, null );
	}
	
	public Cursor fetchAll( SQLiteDatabase db ) {
		return db.query( DATABASE_TABLE, null, null, null, null, null, null);
	}
	
	public void reset( SQLiteDatabase db ) {
		db.execSQL( deleteTable() );
		db.execSQL( createTable() );
	}	
}
