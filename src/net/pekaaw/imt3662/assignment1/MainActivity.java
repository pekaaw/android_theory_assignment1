package net.pekaaw.imt3662.assignment1;

import net.pekaaw.imt3662.oblig1.R;
import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;

public class MainActivity extends Activity {

	DBAdapter mDBAdapter = new DBAdapter( this ); 
	DBtable_headlines mHeadlineTable = new DBtable_headlines();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		final ImageView imageView = (ImageView) findViewById(R.id.button_new_content);
		imageView.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				show_add_menu(v);
			}
		});

	}
	
	@Override
	protected void onStart() {
		showVisitedHeadlines();
		super.onStart();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle presses at the Action bar
		switch(item.getItemId()) {
		case R.id.news:
			Intent intent = new Intent(this, Headline.class);
			startActivity(intent);
			return true;
		default:
			return super.onOptionsItemSelected(item);	
		}
	}
	
	public void show_add_menu(View view) {
		PopupMenu popup = new PopupMenu(this, view);
//		MenuInflater inflater = popup.getMenuInflater();
//		inflater.inflate(R.menu.add, popup.getMenu());
		popup.setOnMenuItemClickListener(
				new PopupMenu.OnMenuItemClickListener() {
					
					public boolean onMenuItemClick(MenuItem item) {
					    switch (item.getItemId()) {
				        case R.id.add_subject:
				            Intent intent = new Intent();
				            intent.setAction(Intent.ACTION_VIEW);
				            intent.setData(Uri.parse("http://vg.no"));
				            startActivity(intent);
				            return true;
				        case R.id.add_person:
				        	// Todo: click-event handling
				            return true;
				        case R.id.add_task:
				        	// Todo: click-event handling
				            return true;
				        default:
				            return false;
					    }
					}
				});
		popup.inflate(R.menu.add);
		popup.show();
		
	}
	
	private void showVisitedHeadlines() {
		// Get a writable database
		SQLiteDatabase db = mDBAdapter.openToWrite();
		Cursor content = mHeadlineTable.fetchAll(db);
		TextView visitedView = (TextView) findViewById(R.id.visited_articles_view);
		
		if( content != null  && content.getCount() != 0) {
			visitedView.setText("");
		}
		
		if( content != null  && content.getCount() > 0 ) {
		
			content.moveToFirst();
			do {
				visitedView.append( "- " + content.getString( 1 ) + "\n" );
			 
			} while( content.moveToNext() );
		}

		content.close();
		mDBAdapter.close();		

	}
	
}
