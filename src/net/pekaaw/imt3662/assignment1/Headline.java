package net.pekaaw.imt3662.assignment1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import net.pekaaw.imt3662.oblig1.R;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

public class Headline extends Activity {

	ArrayAdapter<String> mAdapter = null;
	ArrayList<Uri> mHeadlineUris = new ArrayList<Uri>();

	DBAdapter mDBAdapter = new DBAdapter( this ); 
	DBtable_headlines mHeadlineTable = new DBtable_headlines();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_headline);
		// Show the Up button in the action bar.
		setupActionBar();
		initList();
		//new getHeadlines().execute();
		new getWebHTML().execute("http://hackaday.com");
	}
	
	@Override
	protected void onStart(){
		super.onStart();
		
	}
	
	@Override
	protected void onPause() {
		super.onPause();
		SharedPreferences savedContent = this.getPreferences(Context.MODE_PRIVATE);
		savedContent.edit().putBoolean("hei", true);
	}

	/**
	 * Set up the {@link android.app.ActionBar}.
	 */
	private void setupActionBar() {

		getActionBar().setDisplayHomeAsUpEnabled(true);

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.headline, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			// This ID represents the Home or Up button. In the case of this
			// activity, the Up button is shown. Use NavUtils to allow users
			// to navigate up one level in the application structure. For
			// more details, see the Navigation pattern on Android Design:
			//
			// http://developer.android.com/design/patterns/navigation.html#up-vs-back
			//
			NavUtils.navigateUpFromSameTask(this);
			return true;
		case R.id.button_headline_update:
			new getWebHTML().execute("http://hackaday.com");
		}
		return super.onOptionsItemSelected(item);
	}

	private void initList() {
		/**
		 * get a listview and values with string
		 */
		final ListView listview = (ListView) findViewById(R.id.listview_headlines);

		/**
		 * set action on a click-event
		 * 
		 * Animate a removal and remove the item.
		 */
		listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, final View view,
					final int position, long id) {
				// final String item = (String)
				// parent.getItemAtPosition(position);
				view.animate().setDuration(500).alpha(0)
						.withEndAction(new Runnable() {

							@Override
							public void run() {
								
								// Get a writable database
								SQLiteDatabase db = mDBAdapter.openToWrite();
									
								
								try {
									mHeadlineTable.addEntry(
										db, 
										mAdapter.getItem(position), 
										mHeadlineUris.get(position).toString()
									);
								} 
								catch ( SQLiteException e ) {
									Log.d( "Error: ", e.getMessage() );
								}
								
								mDBAdapter.close();		

								
					            Intent intent = new Intent();
					            intent.setAction(Intent.ACTION_VIEW);
					            intent.setData(mHeadlineUris.get(position));
					            startActivity(intent);

								//mAdapter.remove(mAdapter.getItem(position));
								mAdapter.notifyDataSetChanged();
								view.setAlpha(1);
							}
						});
			}

		}); // end listview.setOnItemClickListener()

	} // end initList();

	private class getWebHTML extends AsyncTask<String, Boolean, String> {
	
		@Override
		protected void onPreExecute() {
			if(mAdapter != null) {
				mAdapter.clear();
				mAdapter.notifyDataSetChanged();
			}
			
			ProgressBar progress = (ProgressBar) findViewById(R.id.load_progress);
			progress.setVisibility(ProgressBar.VISIBLE);
			
			TextView textview = (TextView) findViewById(R.id.load_message);
			textview.setVisibility(TextView.VISIBLE);
			textview.setText(R.string.load_message_request_sent);
		}
		
		@Override
		protected String doInBackground(String... urls) {
			
			assert(urls.length != 1);	// integration check
			
			
			String webHTML = null;

			try {
				URL url = new URL(urls[0]);
				
				HttpURLConnection connection = 
						(HttpURLConnection) url.openConnection();
				connection.setReadTimeout(10000);
				connection.setConnectTimeout(15000);
				connection.setRequestMethod("GET");
				connection.setDoInput(true);
				
				connection.connect();
				
				if(connection.getResponseCode() == HttpURLConnection.HTTP_OK) {
					
					InputStream inputStream = connection.getInputStream();
					
					webHTML = readStream(inputStream);

					// now tell the activity that we've got the content
					publishProgress(true);
				
				}
			} catch (Exception e) {
				e.printStackTrace();
			}

			return webHTML;
		} // end doInBackground()
		
		@Override
		protected void onProgressUpdate(Boolean...booleans) {
			super.onProgressUpdate(booleans);
			if(booleans[0] == true) {
				TextView textview = (TextView) findViewById(R.id.load_message);
				textview.setText(R.string.load_message_response_received);
			}
		}
		
		@Override
		protected void onPostExecute(String webContent) {
			ProgressBar progress = (ProgressBar) findViewById(R.id.load_progress);
			progress.setVisibility(ProgressBar.GONE);
			
			TextView textview = (TextView) findViewById(R.id.load_message);

			if(webContent == null) {
				textview.setText(R.string.load_message_failed);
				
				ConnectivityManager cm = (ConnectivityManager) 
				       getSystemService(Context.CONNECTIVITY_SERVICE);
				
				NetworkInfo networkInfo = cm.getActiveNetworkInfo();
				 // if no network is available networkInfo will be null
				 // otherwise check if we are connected
				
				if(networkInfo != null && networkInfo.isConnected() == true) {
					return;
				}
					
				textview.append("\n" + getResources().getString(R.string.network_disconnected));
				
				return;
			}
			
			textview.setVisibility(TextView.GONE);
			fillHeadlineList(webContent);
		}
		
		private String readStream(InputStream inputStream) {
			String stream = "";
			BufferedReader reader = null;
			try {
				reader = new BufferedReader(new InputStreamReader(inputStream));
				String line = "";
				while ((line = reader.readLine()) != null) {
					stream += line;
				}
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				if(reader != null) {
					try {
						reader.close();
					} catch(IOException e) {
						e.printStackTrace();
					}
				}
			}
						
			return stream;
		} // end readStream()
	} //end getSite() AsyncTask
	
	/**
	 * Fill a ListView with headlines from Hackaday.com*
	 * <p>
	 * Extract headlines from the HTML-string called webContent. Find also 
	 * links to the article connected to the headline and store those in 
	 * mHeadlineUrls. These uri's will later be used in an onclick-event
	 * that will bring the user to the page in the standard browser.
	 * <p>
	 * *following this sites html-structure
	 * 
	 * @param webContent String with html-code
	 */
	private void fillHeadlineList(String webContent) {
		
		ArrayList<String> headlines = new ArrayList<String>();
		mHeadlineUris = new ArrayList<Uri>();
		Document doc = Jsoup.parse(webContent);
		
		Elements h2lines = doc.select("h2.entry-title");
		String uriString = null;
		Uri uri = null;
		
		for(Element h2line : h2lines) {
			headlines.add(h2line.text());
			
			uriString = h2line.select("a:first-child").first().attr("abs:href");
			try{
				uri = Uri.parse(uriString);
			} catch(NullPointerException e) {
				uri = null;
			}
			mHeadlineUris.add(uri);
			
		}
		
		final ListView listview = (ListView) findViewById(R.id.listview_headlines);

		final ArrayAdapter<String> adapter = new ArrayAdapter<String>(
				getBaseContext(), android.R.layout.simple_list_item_1,
				headlines);

		mAdapter = adapter;
		listview.setAdapter(adapter);

	} // end fillHeadlineList
	

}
