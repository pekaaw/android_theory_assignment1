package net.pekaaw.imt3662.assignment1;

import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DBAdapter{
	
	public static final String DATABASE_NAME = "headlines";
	public static final int DATABASE_VERSION = 1;
	
	SQLiteDatabase mDatabase;
	DatabaseHelper mDbHelper;
	Context mContext;
	
	public DBAdapter( Context context ) {
		mContext = context;
	}
	
	private static class DatabaseHelper extends SQLiteOpenHelper {

	    DatabaseHelper( Context context ) {
	    	super( context, DATABASE_NAME, null, DATABASE_VERSION );
	    }

	    /**
	     * onCreate
	     * 
	     * Define what to do when creating the database. This will
	     * only be executed when the database is created and updated.
	     * 
	     * @return void
	     */
		@Override
		public void onCreate( SQLiteDatabase db ) {
			db.execSQL( DBtable_headlines.createTable() );
		}

		/**
		 * onUpgrade
		 * 
		 * Define what to do when upgrading the database.
		 * In this implementation, the database is deleted and recreated
		 * by running onCreate() again.
		 * 
		 * @return void
		 */
		@Override
		public void onUpgrade( SQLiteDatabase db, int oldVersion, int newVersion ) {
			onCreate( db );
		}
	
		// end of class DatabaseHelper
	}
	
	/**
	 * Make the database ready for reading
	 * 
	 * @return this The database 'object' ready for reading
	 * @throws SQLException Upon failure
	 */
	public SQLiteDatabase openToRead() throws SQLException {
		this.mDbHelper = new DatabaseHelper( mContext );
		this.mDatabase = this.mDbHelper.getReadableDatabase();
		return this.mDatabase;
	}
	
	/**
	 * Make the database ready for writing
	 * 
	 * @return SQLiteDatabase A writeable database
	 * @throws SQLException Upon failure
	 */
	public SQLiteDatabase openToWrite() throws SQLException {
		this.mDbHelper = new DatabaseHelper( mContext );
		this.mDatabase = this.mDbHelper.getWritableDatabase();
		return this.mDatabase;
	}
	
	public void reset() {
		mDbHelper.onDowngrade(this.openToWrite(), 1, 1);
	}
	
	/**
	 * Close the database by asking the SQLiteHelper to close.
	 * 
	 * @return void
	 */
	public void close() {
		mDbHelper.close();
	}

	// end of classDBAdapter
}